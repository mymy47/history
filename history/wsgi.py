"""
WSGI config for history project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.8/howto/deployment/wsgi/
"""

import os
import sys

sys.path.append('/home/apps/history')
sys.path.append('/home/apps/history/history')

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "history.settings")

# serve django via WSGI
from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()
