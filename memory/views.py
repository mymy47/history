from django import forms
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.shortcuts import render_to_response
from memory.models import Memory


def home(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('new_memory'))
    memories = Memory.objects.all()
    return render(request, 'index.html', {'memories': memories})


class MemoryForm(forms.ModelForm):
    class Meta:
        model = Memory
        exclude = ('user',)


@login_required
def new_memory(request):
    if request.method == 'POST':
        form = MemoryForm(request.POST.copy(), request.FILES)
        if form.is_valid():
            obj = form.save()
            obj.user = request.user
            obj.save()
            messages.success(request, u"خاطره با موفقیت ثبت شد.")

            return HttpResponseRedirect(reverse('home'))
    else:
        form = MemoryForm()
    return render(request, 'new_memory.html', {'form': form})


def page(request, mem_id):
    mem = get_object_or_404(Memory, id=mem_id)
    return render(request, 'page.html', {'mem': mem})
