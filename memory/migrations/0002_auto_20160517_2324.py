# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('memory', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='memory',
            name='title',
            field=models.CharField(default='', max_length=255, verbose_name='عنوان'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='memory',
            name='image',
            field=models.ImageField(null=True, upload_to='images', blank=True, verbose_name='تصویر'),
        ),
        migrations.AlterField(
            model_name='memory',
            name='user',
            field=models.ForeignKey(null=True, to=settings.AUTH_USER_MODEL, verbose_name='کاربر'),
        ),
        migrations.AlterField(
            model_name='memory',
            name='video',
            field=models.FileField(null=True, upload_to='videos', blank=True, verbose_name='ویدیو'),
        ),
    ]
