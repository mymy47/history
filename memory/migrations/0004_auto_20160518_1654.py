# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('memory', '0003_auto_20160518_1251'),
    ]

    operations = [
        migrations.AddField(
            model_name='memory',
            name='date',
            field=models.CharField(verbose_name='تاریخ', max_length=255, null=True),
        ),
        migrations.AddField(
            model_name='memory',
            name='place',
            field=models.CharField(verbose_name='محل رخداد', max_length=255, null=True),
        ),
        migrations.AlterField(
            model_name='memory',
            name='video',
            field=models.FileField(blank=True, verbose_name='عکس یا ویدیو', null=True, upload_to='videos'),
        ),
    ]
