# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Memory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(verbose_name='تصویر', upload_to='extra_images', blank=True, null=True)),
                ('created_on', models.DateTimeField(verbose_name='تاریخ ایجاد', auto_now_add=True)),
                ('text', models.TextField()),
                ('video', models.FileField(verbose_name='ویدیو', upload_to='', blank=True, null=True)),
                ('user', models.ForeignKey(verbose_name='کاربر', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'خاطره',
                'verbose_name_plural': 'خاطره ها',
            },
        ),
    ]
