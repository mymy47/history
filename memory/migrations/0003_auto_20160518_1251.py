# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('memory', '0002_auto_20160517_2324'),
    ]

    operations = [
        migrations.AlterField(
            model_name='memory',
            name='text',
            field=models.TextField(verbose_name='متن'),
        ),
    ]
