from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.db import models

from account.models import Profile


class Memory(models.Model):
    title = models.CharField(verbose_name="عنوان", max_length=255)
    date = models.CharField(verbose_name="تاریخ", max_length=255, null=True)
    place = models.CharField(verbose_name="محل رخداد", max_length=255, null=True)
    user = models.ForeignKey(Profile, verbose_name="کاربر", null=True)
    text = models.TextField(verbose_name="متن")
    image = models.ImageField(verbose_name=u"تصویر", upload_to="images", null=True, blank=True)
    video = models.FileField(verbose_name="عکس یا ویدیو", null=True, blank=True, upload_to='videos')
    created_on = models.DateTimeField(verbose_name="تاریخ ایجاد", auto_now_add=True)

    class Meta:
        verbose_name = "خاطره"
        verbose_name_plural = "خاطره ها"

    def __str__(self):
        return self.title

    @property
    def image_url(self):
        if not self.image:
            return ''
        return settings.SITE_URL + self.image.url
