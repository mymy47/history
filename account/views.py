from django import forms
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.decorators.cache import never_cache

from account.models import Profile


@never_cache
def logout(request):
    from django.contrib.auth import logout

    logout(request)
    return HttpResponseRedirect(reverse('home'))


def login(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('new_memory'))
    from django.contrib.auth import authenticate, login

    if request.method == 'POST':

        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is None or not user.is_active:
            messages.error(request, u"نام کاربری یا گذرواژه نادرست است.")
        else:
            login(request, user)
            next_page = request.GET.get('next', '/')
            if next_page:
                return HttpResponseRedirect(reverse('new_memory'))

    context = {
        'app_path': request.get_full_path(),
        'next': request.get_full_path(),
    }

    return render(request, 'account/login.html', context)


class SignUpForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('email', 'password', 'phone', 'depart', 'inter_year', 'first_name')

    password = forms.CharField(label="گذرواژه", widget=forms.PasswordInput)


def signup(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect(reverse('new_memory'))
    from django.contrib.auth import authenticate, login

    error = False
    if request.method == 'POST':
        form = SignUpForm(request.POST.copy())
        if form.is_valid():
            profile = form.save(commit=False)
            password = form.cleaned_data.get('password')
            profile.set_password(password)
            profile.username = form.cleaned_data.get('email')
            profile.save()
            # messages.success(request, u"ثبت نام شما با موفقیت انجانسببم شد. اکنون می توانید وارد شوید.")
            # next_page = request.GET.get('next') or '/'

            user = authenticate(username=profile.username, password=password)
            login(request, user)

            return HttpResponseRedirect(reverse('new_memory'))
        else:
            error = True
    else:
        form = SignUpForm()
    return render(request, 'index.html', {'form': form, 'error': error})
