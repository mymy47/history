from django.contrib.auth.models import AbstractUser
from django.db import models


class Profile(AbstractUser):
    phone = models.CharField(verbose_name="شماره تلفن", max_length=255, null=True, blank=True)
    depart = models.CharField(verbose_name="دانشکده", max_length=255, null=True, blank=True)
    inter_year = models.CharField(verbose_name="سال ورود", max_length=255, null=True, blank=True)

    def __str__(self):
        return self.username
