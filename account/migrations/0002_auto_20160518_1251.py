# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='depart',
            field=models.CharField(verbose_name='دانشکده', default='', max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='profile',
            name='inter_year',
            field=models.CharField(verbose_name='سال ورود', default='', max_length=255),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='profile',
            name='phone',
            field=models.CharField(verbose_name='شماره تلفن', default='', max_length=255),
            preserve_default=False,
        ),
    ]
