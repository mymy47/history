# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0002_auto_20160518_1251'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='depart',
            field=models.CharField(null=True, verbose_name='دانشکده', max_length=255, blank=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='inter_year',
            field=models.CharField(null=True, verbose_name='سال ورود', max_length=255, blank=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='phone',
            field=models.CharField(null=True, verbose_name='شماره تلفن', max_length=255, blank=True),
        ),
    ]
