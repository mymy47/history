from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from account.models import Profile
from memory.models import Memory

admin.site.register(Profile, UserAdmin)
admin.site.register(Memory)
